#include "GEM/Modules/interface/ptZreweightInterface.h"


// Constructors


ptZreweightInterface::ptZreweightInterface (
      std::string filename, std::string target_filename,
      std::string histoname, std::string target_histoname,
      bool norm, bool verbose
    )
{
  TFile* file = TFile::Open(filename.c_str());
  histo = (TH1D*) file->Get(histoname.c_str());

  TFile* target_file = TFile::Open(target_filename.c_str());
  target_histo = (TH1D*) target_file->Get(target_histoname.c_str());
  
  reweighter = WeightCalculatorFromHistogram(histo, target_histo, norm, true, verbose);
}

// Destructor
ptZreweightInterface::~ptZreweightInterface() {}

float ptZreweightInterface::get_weight(float ptZ) {
  if ( histo->GetXaxis()->FindBin(ptZ) > histo->GetNbinsX() ) return 1;
  else return reweighter.getWeight(ptZ);  
}
