#include "GEM/Modules/interface/DiMuonSelector.h"

// Constructors
DiMuonSelector::DiMuonSelector() {};

// Destructor
DiMuonSelector::~DiMuonSelector() {}

muon_output DiMuonSelector::GoodZMuons(
				       fRVec& Muon_pt, fRVec& Muon_tunepRelPt, fRVec& Muon_ptErr, fRVec& Muon_eta, fRVec& Muon_phi, 
				       iRVec& Muon_highPtId, fRVec& Muon_tkRelIso, fRVec& Muon_dxy, iRVec& Muon_charge )
{
  Int_t goodMu1 = -1;
  Int_t goodMu2 = -1;
  Float_t kappa1 = -999.0;
  Float_t kappa2 = -999.0;
  Float_t invM = -999.0;
  Float_t ptZ = -999.0;

  for(size_t imu1 = 0; imu1 < Muon_pt.size(); imu1 ++){

    //if( Muon_tunepRelPt[imu1]*Muon_pt[imu1] < 53.0 || Muon_highPtId[imu1] != 2 || fabs(Muon_eta[imu1]) > 2.4 || Muon_ptErr[imu1] > 0.3*Muon_tunepRelPt[imu1]*Muon_pt[imu1] || Muon_tkRelIso[imu1] > 0.1 || fabs(Muon_dxy[imu1]) > 0.02 ) // In Run2 ==> For Run3 remove ptErr cut, already in High-pt ID.
    if( Muon_tunepRelPt[imu1]*Muon_pt[imu1] < 53.0 || Muon_highPtId[imu1] != 2 || fabs(Muon_eta[imu1]) > 2.4 || Muon_tkRelIso[imu1] > 0.1 || fabs(Muon_dxy[imu1]) > 0.02 )
      continue;

    for(size_t imu2 = imu1+1; imu2 < Muon_pt.size(); imu2++){

      if(Muon_charge[imu1]*Muon_charge[imu2] == 1) 
	continue;

      //if( Muon_tunepRelPt[imu2]*Muon_pt[imu2] < 25.0 || Muon_highPtId[imu2] != 2 || fabs(Muon_eta[imu2]) > 2.4 || Muon_ptErr[imu2] > 0.3*Muon_tunepRelPt[imu2]*Muon_pt[imu2] || Muon_tkRelIso[imu2] > 0.1 || fabs(Muon_dxy[imu2]) > 0.02 ) // In Run2 ==> For Run3 remove ptErr cut, already in High-pt ID.
      if( Muon_tunepRelPt[imu2]*Muon_pt[imu2] < 25.0 || Muon_highPtId[imu2] != 2 || fabs(Muon_eta[imu2]) > 2.4 || Muon_tkRelIso[imu2] > 0.1 || fabs(Muon_dxy[imu2]) > 0.02 )
	continue;

      Float_t zMass = sqrt( 2*Muon_tunepRelPt[imu1]*Muon_pt[imu1]*Muon_tunepRelPt[imu2]*Muon_pt[imu2]*( cosh(Muon_eta[imu1] - Muon_eta[imu2]) - cos(deltaPhi(Muon_phi[imu1], Muon_phi[imu2])) ) );

      if(zMass < 55.0)
	continue;

      goodMu1 = imu1;
      goodMu2 = imu2;
      kappa1 = 1000*Muon_charge[imu1]/(Muon_tunepRelPt[imu1]*Muon_pt[imu1]);
      kappa2 = 1000*Muon_charge[imu2]/(Muon_tunepRelPt[imu2]*Muon_pt[imu2]);
      invM = zMass;
      ptZ = sqrt((Muon_tunepRelPt[imu1]*Muon_pt[imu1])*(Muon_tunepRelPt[imu1]*Muon_pt[imu1])+(Muon_tunepRelPt[imu2]*Muon_pt[imu2])*(Muon_tunepRelPt[imu2]*Muon_pt[imu2])+2*(Muon_tunepRelPt[imu1]*Muon_pt[imu1])*(Muon_tunepRelPt[imu2]*Muon_pt[imu2])*(cos(Muon_phi[imu1])*cos(Muon_phi[imu2])+sin(Muon_phi[imu1])*sin(Muon_phi[imu2])));

      return muon_output({goodMu1, goodMu2, kappa1, kappa2, invM, ptZ});

    } // Loop over Second Muon
  } // Loop over First Muon

  return muon_output({goodMu1, goodMu2, kappa1, kappa2, invM, ptZ});

}
