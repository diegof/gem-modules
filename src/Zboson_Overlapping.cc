#include "GEM/Modules/interface/Zboson_Overlapping.h"

// Constructors                                                                                                                                      
ZonshellOverlap::ZonshellOverlap() {};

// Destructor                                                                                                                                        
ZonshellOverlap::~ZonshellOverlap() {}

bool ZonshellOverlap::ZonshellStitcher(iRVec& LHEPart_pdgId, fRVec& LHEPart_pt, fRVec& LHEPart_eta, fRVec& LHEPart_phi, fRVec& LHEPart_mass){

  TLorentzVector mu1, mu2, Z;
  bool rejectEvent = false;

  for(size_t iLHE1 = 0; iLHE1 < LHEPart_pdgId.size(); iLHE1++){

    if(abs(LHEPart_pdgId[iLHE1]) != 13 && abs(LHEPart_pdgId[iLHE1]) != 15) continue;

    for(size_t iLHE2 = iLHE1+1; iLHE2 < LHEPart_pdgId.size(); iLHE2++){

      if( (abs(LHEPart_pdgId[iLHE2]) == 13 || abs(LHEPart_pdgId[iLHE2]) == 15) && (LHEPart_pdgId[iLHE1]*LHEPart_pdgId[iLHE2] < 0) ){

        mu1.SetPtEtaPhiM(LHEPart_pt[iLHE1], LHEPart_eta[iLHE1], LHEPart_phi[iLHE1], LHEPart_mass[iLHE1]);
        mu2.SetPtEtaPhiM(LHEPart_pt[iLHE2], LHEPart_eta[iLHE2], LHEPart_phi[iLHE2], LHEPart_mass[iLHE2]);

        Z = mu1 + mu2;

        if(Z.Pt() > 100.0) {
          rejectEvent = true;
	  return rejectEvent;
	}

      }

    } // Loop over LHE particles 2                                                                                                                   

  } // Loop over LHE particles 1                                                                                                                     


  return rejectEvent;

}


// Constructors
ZboostedOverlap::ZboostedOverlap() {};

// Destructor
ZboostedOverlap::~ZboostedOverlap() {}

bool ZboostedOverlap::ZboostedStitcher(iRVec& LHEPart_pdgId, fRVec& LHEPart_pt, fRVec& LHEPart_eta, fRVec& LHEPart_phi, fRVec& LHEPart_mass){
  
  TLorentzVector mu1, mu2, Z;
  bool rejectEvent = false;

  for(size_t iLHE1 = 0; iLHE1 < LHEPart_pdgId.size(); iLHE1++){

    if(abs(LHEPart_pdgId[iLHE1]) != 13 && abs(LHEPart_pdgId[iLHE1]) != 15) continue;

    for(size_t iLHE2 = iLHE1+1; iLHE2 < LHEPart_pdgId.size(); iLHE2++){

      if( (abs(LHEPart_pdgId[iLHE2]) == 13 || abs(LHEPart_pdgId[iLHE2]) == 15) && (LHEPart_pdgId[iLHE1]*LHEPart_pdgId[iLHE2] < 0) ){

	mu1.SetPtEtaPhiM(LHEPart_pt[iLHE1], LHEPart_eta[iLHE1], LHEPart_phi[iLHE1], LHEPart_mass[iLHE1]);
	mu2.SetPtEtaPhiM(LHEPart_pt[iLHE2], LHEPart_eta[iLHE2], LHEPart_phi[iLHE2], LHEPart_mass[iLHE2]);

	Z = mu1 + mu2;

	if(Z.M() > 120.0){
	  rejectEvent = true;
	  return rejectEvent;
	}

      }
 
    } // Loop over LHE Particles 2

  } // Loop over LHE Particles 1

  return rejectEvent;

}
