#include "GEM/Modules/interface/GenPart_momPrompt.h"

// Constructors
GenPart_momPrompt::GenPart_momPrompt() {};

// Destructor
GenPart_momPrompt::~GenPart_momPrompt() {}

std::vector<int> GenPart_momPrompt::momPrompt(iRVec& GenPart_pdgId, iRVec& GenPart_genPartIdxMother){
  
  std::vector<int> final_momIdx;
  int son, mom;

  for(size_t iGen = 0; iGen < GenPart_pdgId.size(); iGen++){

    if(GenPart_genPartIdxMother[iGen] != -1){
      if( GenPart_pdgId[GenPart_genPartIdxMother[iGen]] != GenPart_pdgId[iGen] )
	final_momIdx.push_back(GenPart_genPartIdxMother[iGen]);
      else {
	son = iGen;
	mom = GenPart_genPartIdxMother[iGen];

	while(GenPart_pdgId[mom] == GenPart_pdgId[son] && GenPart_genPartIdxMother[mom] != -1){
	  son = mom;
	  mom = GenPart_genPartIdxMother[son];
	}
	if(GenPart_pdgId[son] == GenPart_pdgId[mom]) final_momIdx.push_back(-1);
	else final_momIdx.push_back(mom);
      }
    }
    else
      final_momIdx.push_back(-1);
  } // Loop over Gen Particles

  return final_momIdx;

}
