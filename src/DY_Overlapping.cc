#include "GEM/Modules/interface/DY_Overlapping.h"

// Constructors
DY_TauOverlapping::DY_TauOverlapping() {};

// Destructor
DY_TauOverlapping::~DY_TauOverlapping() {}

bool DY_TauOverlapping::DYTauStitcher(iRVec& GenPart_pdgId, iRVec& GenPart_genPartIdxMotherPrompt, const int& LHE_HT, iRVec& LHEPart_pdgId, fRVec& LHEPart_mass, fRVec& LHEPart_pt, fRVec& LHEPart_eta, fRVec& LHEPart_phi){
  
  TLorentzVector mu1, mu2, Z;
  bool rejectEvent = false;
  int genMu1_idx = -1, genMu2_idx = -1;

  /* Cut on HT < 200 for mZ < 120 */
  for(size_t iLHE1 = 0; iLHE1 < LHEPart_pdgId.size(); iLHE1++){
    if(abs(LHEPart_pdgId[iLHE1]) != 13) continue;

    for(size_t iLHE2 = iLHE1+1; iLHE2 < LHEPart_pdgId.size(); iLHE2++){
      if(abs(LHEPart_pdgId[iLHE2]) != 13) continue;
     
      if(LHEPart_pdgId[iLHE1]*LHEPart_pdgId[iLHE2] != 1){

	mu1.SetPtEtaPhiM(LHEPart_pt[iLHE1], LHEPart_eta[iLHE1], LHEPart_phi[iLHE1], LHEPart_mass[iLHE1]);
	mu2.SetPtEtaPhiM(LHEPart_pt[iLHE2], LHEPart_eta[iLHE2], LHEPart_phi[iLHE2], LHEPart_mass[iLHE2]);
	Z = mu1 + mu2;

	if(Z.M() < 120.0 && LHE_HT > 200.0){
	  rejectEvent = true;
	  return rejectEvent;
	}
      }
 
    } // Loop over LHE Particles 2
  } // Loop over LHE Particles 1

  /* Skim Z -> mumu events (leave only Taus) */
  for(size_t iGen1 = 0; iGen1 < GenPart_pdgId.size(); iGen1++){

    if(abs(GenPart_pdgId[iGen1]) == 13 && GenPart_genPartIdxMotherPrompt[iGen1] > -1){
      if(abs(GenPart_pdgId[GenPart_genPartIdxMotherPrompt[iGen1]]) == 23) genMu1_idx = iGen1;
      else continue;
    }
    else continue;

    for(size_t iGen2 = iGen1+1; iGen2 < GenPart_pdgId.size(); iGen2++){

      if(abs(GenPart_pdgId[iGen2]) == 13 && GenPart_genPartIdxMotherPrompt[iGen2] > -1){
	if(abs(GenPart_pdgId[GenPart_genPartIdxMotherPrompt[iGen2]]) == 23) genMu2_idx = iGen2;
	else continue;
      }
      else continue;
      
      if(genMu1_idx != -1 && genMu2_idx != -1 && GenPart_pdgId[genMu1_idx]*GenPart_pdgId[genMu2_idx] != 1){
	rejectEvent = true;
	return rejectEvent;
      }
     
    } // Loop over Gen Particles 2
  } // Loop over Gen Particles 1

  return rejectEvent;

}


// Constructors
DYHToverlapping::DYHToverlapping() {};

// Destructor
DYHToverlapping::~DYHToverlapping() {}

bool DYHToverlapping::DYHTstitcher(iRVec& LHEPart_pdgId, fRVec& LHEPart_mass, fRVec& LHEPart_pt, fRVec& LHEPart_eta, fRVec& LHEPart_phi){
  
  TLorentzVector mu1, mu2, Z;
  bool rejectEvent = false;

  for(size_t iLHE1 = 0; iLHE1 < LHEPart_pdgId.size(); iLHE1++){
    if(abs(LHEPart_pdgId[iLHE1]) != 13) continue;

    for(size_t iLHE2 = iLHE1+1; iLHE2 < LHEPart_pdgId.size(); iLHE2++){
      if(abs(LHEPart_pdgId[iLHE2]) != 13) continue;
     
      if(LHEPart_pdgId[iLHE1]*LHEPart_pdgId[iLHE2] != 1){

	mu1.SetPtEtaPhiM(LHEPart_pt[iLHE1], LHEPart_eta[iLHE1], LHEPart_phi[iLHE1], LHEPart_mass[iLHE1]);
	mu2.SetPtEtaPhiM(LHEPart_pt[iLHE2], LHEPart_eta[iLHE2], LHEPart_phi[iLHE2], LHEPart_mass[iLHE2]);
	Z = mu1 + mu2;

	if(Z.M() > 120.0){
	  rejectEvent = true;
	  return rejectEvent;
	}
      }
 
    } // Loop over LHE Particles 2
  } // Loop over LHE Particles 1

  return rejectEvent;

}
