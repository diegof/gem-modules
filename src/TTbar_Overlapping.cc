#include "GEM/Modules/interface/TTbar_Overlapping.h"

// Constructors
TTbar_Overlapping::TTbar_Overlapping() {};

// Destructor
TTbar_Overlapping::~TTbar_Overlapping() {}

bool TTbar_Overlapping::TTstitcher(iRVec& GenPart_pdgId, fRVec& GenPart_pt, fRVec& GenPart_eta, fRVec& GenPart_phi, fRVec& GenPart_mass){
  
  TLorentzVector top1, top2, tt;
  int gent1_idx = -1, gent2_idx = -1;
  bool rejectEvent = false;

  for(size_t iGen1 = 0; iGen1 < GenPart_pdgId.size(); iGen1++){

    if(abs(GenPart_pdgId[iGen1]) == 6)
      gent1_idx = iGen1;
    else continue;

    for(size_t iGen2 = iGen1+1; iGen2 < GenPart_pdgId.size(); iGen2++){

      if(abs(GenPart_pdgId[iGen2]) == 6)
	gent2_idx = iGen2;
      else continue;
      
      if(GenPart_pdgId[gent1_idx]*GenPart_pdgId[gent2_idx] != 1){

	top1.SetPtEtaPhiM(GenPart_pt[gent1_idx], GenPart_eta[gent1_idx], GenPart_phi[gent1_idx], GenPart_mass[gent1_idx]);
	top2.SetPtEtaPhiM(GenPart_pt[gent2_idx], GenPart_eta[gent2_idx], GenPart_phi[gent2_idx], GenPart_mass[gent2_idx]);
	tt = top1 + top2;

	if(tt.M() > 700.0){
	  rejectEvent = true;
	  return rejectEvent;
	}
      }
     
    } // Loop over Gen Particles 2
  } // Loop over Gen Particles 1

  return rejectEvent;

}
