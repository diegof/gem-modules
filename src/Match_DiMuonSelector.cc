#include "GEM/Modules/interface/Match_DiMuonSelector.h"

// Constructors
Match_DiMuonSelector::Match_DiMuonSelector() {};

// Destructor
Match_DiMuonSelector::~Match_DiMuonSelector() {}

muon_output_v2 Match_DiMuonSelector::GoodZMuons(
				       fRVec& Muon_pt, fRVec& Muon_tunepRelPt, fRVec& Muon_ptErr, fRVec& Muon_eta, fRVec& Muon_phi, 
				       iRVec& Muon_highPtId, fRVec& Muon_tkRelIso, fRVec& Muon_dxy, iRVec& Muon_charge )
{
  std::vector<int> goodMu1;
  std::vector<int> goodMu2;
  std::vector<float> invM;

  for(size_t imu1 = 0; imu1 < Muon_pt.size(); imu1 ++){

    if( Muon_tunepRelPt[imu1]*Muon_pt[imu1] < 53.0 || Muon_highPtId[imu1] != 2 || fabs(Muon_eta[imu1]) > 2.4 || Muon_ptErr[imu1] > 0.3*Muon_tunepRelPt[imu1]*Muon_pt[imu1] || Muon_tkRelIso[imu1] > 0.1 || fabs(Muon_dxy[imu1]) > 0.02 )
      continue;

    for(size_t imu2 = imu1+1; imu2 < Muon_pt.size(); imu2++){

      if(Muon_charge[imu1]*Muon_charge[imu2] == 1) 
	continue;
      if( Muon_tunepRelPt[imu2]*Muon_pt[imu2] < 25.0 || Muon_highPtId[imu2] != 2 || fabs(Muon_eta[imu2]) > 2.4 || Muon_ptErr[imu2] > 0.3*Muon_tunepRelPt[imu2]*Muon_pt[imu2] || Muon_tkRelIso[imu2] > 0.1 || fabs(Muon_dxy[imu2]) > 0.02 )
	continue;

      float zMass = sqrt( 2*Muon_tunepRelPt[imu1]*Muon_pt[imu1]*Muon_tunepRelPt[imu2]*Muon_pt[imu2]*( cosh(Muon_eta[imu1] - Muon_eta[imu2]) - cos(deltaPhi(Muon_phi[imu1], Muon_phi[imu2])) ) );
      if(zMass < 55.0)
	continue;

      goodMu1.push_back(imu1);
      goodMu2.push_back(imu2);
      invM.push_back(zMass);

    } // Loop over Second Muon
  } // Loop over First Muon

  if(goodMu1.size() > 0){
    return muon_output_v2({goodMu1, goodMu2, invM});
  }else{
    return muon_output_v2({{-1}, {-1}, {-1.0}});
  }

}


Z_output Match_DiMuonSelector::Zmatching(
					 iRVec& GenPart_pdgId, iRVec& GenPart_genPartIdxMotherPrompt, fRVec& GenPart_eta, 
					 fRVec& GenPart_phi, fRVec& Muon_eta, fRVec& Muon_phi, iRVec& Muon_charge )
{
  std::vector<int> Zmu1_idx;
  std::vector<int> Zmu2_idx;
  std::vector<bool> isZ;

  bool mu1_match = false;
  bool mu2_match = false;

  for(size_t imu1 = 0; imu1 < Muon_phi.size(); imu1++){
    for(size_t iGen = 0; iGen < GenPart_phi.size(); iGen++){
      
      if(abs(GenPart_pdgId[iGen]) == 13 && GenPart_genPartIdxMotherPrompt[iGen] > -1){
	if( deltaR(GenPart_eta[iGen], Muon_eta[imu1], GenPart_phi[iGen], Muon_phi[imu1]) < 0.1 && abs(GenPart_pdgId[GenPart_genPartIdxMotherPrompt[iGen]]) == 23 ){
	  mu1_match = true;
	}
      }
    } // Loop over Gen Particles

    for(size_t imu2 = imu1+1; imu2 < Muon_phi.size(); imu2++){

      for(size_t jGen = 0; jGen < GenPart_phi.size(); jGen++){
      
	if(abs(GenPart_pdgId[jGen]) == 13 && GenPart_genPartIdxMotherPrompt[jGen] > -1){
	  if( deltaR(GenPart_eta[jGen], Muon_eta[imu2], GenPart_phi[jGen], Muon_phi[imu2]) < 0.1 && abs(GenPart_pdgId[GenPart_genPartIdxMotherPrompt[jGen]]) == 23 ){
	    mu2_match = true;
	  }
	}
      } // Loop over Gen Particles

      if(Muon_charge[imu1]*Muon_charge[imu2] != 1 && mu1_match && mu2_match){
	isZ.push_back(true);
      }else{
	isZ.push_back(false);
      }

      Zmu1_idx.push_back(imu1);
      Zmu2_idx.push_back(imu2);

    } // Loop over Second Muon
  } // Loop over First Muon

  return Z_output({Zmu1_idx, Zmu2_idx, isZ});

}

float Match_DiMuonSelector::deltaR(float eta1, float eta2, float phi1, float phi2){
  float deta = eta1 - eta2;
  float dphi = deltaPhi(phi1, phi2);

  return sqrt(deta*deta + dphi*dphi);
}
