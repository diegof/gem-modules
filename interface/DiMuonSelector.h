#ifndef DiMuonSelector_h
#define DiMuonSelector_h

// ------------------------------------------------------------------------------ //
//                                                                                //
//   class DiMuonSelector                                                         //
//                                                                                //
// ------------------------------------------------------------------------------ //

// Standard libraries
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

typedef const ROOT::VecOps::RVec<float> fRVec;
typedef const ROOT::VecOps::RVec<bool> bRVec;
typedef const ROOT::VecOps::RVec<int> iRVec;

struct muon_output {
    Int_t mu1_index;
    Int_t mu2_index;
    Float_t kappa_mu1;
    Float_t kappa_mu2;
    Float_t DiMuon_invM;
    Float_t DiMuon_pt;
};


class DiMuonSelector {

  public:
    DiMuonSelector ();
    ~DiMuonSelector ();

    muon_output GoodZMuons(
			   fRVec& Muon_pt, fRVec& Muon_tunepRelPt, fRVec& Muon_ptErr, fRVec& Muon_eta, fRVec& Muon_phi, 
			   iRVec& Muon_highPtId, fRVec& Muon_tkRelIso, fRVec& Muon_dxy, iRVec& Muon_charge );

  private:

};

#endif
