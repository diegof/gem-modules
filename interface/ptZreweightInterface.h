#ifndef ptZreweightInterface_h
#define ptZreweightInterface_h

// Standard libraries
#include <vector>
#include <string>
#include <cmath>

#include <TH1.h>
#include <TFile.h>

#include "PhysicsTools/NanoAODTools/interface/WeightCalculatorFromHistogram.h"

// ptZreweightInterface class
class ptZreweightInterface {
  public:
  ptZreweightInterface ();
    ptZreweightInterface (
      std::string filename, std::string target_filename,
      std::string histoname, std::string target_histoname,
      bool norm, bool verbose
    );
    ~ptZreweightInterface ();
    float get_weight(float ptZ);

  private:
    WeightCalculatorFromHistogram reweighter;
    TH1D* histo;
    TH1D* target_histo;

};

#endif // ptZreweightInterface_h
