#ifndef TTbar_Overlapping_h
#define TTbar_Overlapping_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class TTbar_Overlapping                                                                                      //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

typedef const ROOT::VecOps::RVec<float> fRVec;
typedef const ROOT::VecOps::RVec<bool> bRVec;
typedef const ROOT::VecOps::RVec<int> iRVec;


class TTbar_Overlapping {

  public:
    TTbar_Overlapping ();
    ~TTbar_Overlapping ();

    bool TTstitcher(iRVec& GenPart_pdgId, fRVec& GenPart_pt, fRVec& GenPart_eta, fRVec& GenPart_phi, fRVec& GenPart_mass);

  private:

};

#endif
