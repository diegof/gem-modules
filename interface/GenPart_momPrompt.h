#ifndef GenPart_momPrompt_h
#define GenPart_momPrompt_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class GenPart_momPrompt                                                                                      //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>


typedef const ROOT::VecOps::RVec<float> fRVec;
typedef const ROOT::VecOps::RVec<bool> bRVec;
typedef const ROOT::VecOps::RVec<int> iRVec;


class GenPart_momPrompt {

  public:
    GenPart_momPrompt ();
    ~GenPart_momPrompt ();

    std::vector<int> momPrompt(iRVec& GenPart_pdgId, iRVec& GenPart_genPartIdxMother);

  private:

};

#endif
