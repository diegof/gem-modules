#ifndef DY_Overlapping_h
#define DY_Overlapping_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   classes DY_TauOverlapping, DYHToverlapping ==> Used only in Run 2                                            //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>


typedef const ROOT::VecOps::RVec<float> fRVec;
typedef const ROOT::VecOps::RVec<bool> bRVec;
typedef const ROOT::VecOps::RVec<int> iRVec;


class DY_TauOverlapping {

  public:
    DY_TauOverlapping ();
    ~DY_TauOverlapping ();

    bool DYTauStitcher(iRVec& GenPart_pdgId, iRVec& GenPart_genPartIdxMotherPrompt, const int& LHE_HT, iRVec& LHEPart_pdgId, fRVec& LHEPart_mass, fRVec& LHEPart_pt, fRVec& LHEPart_eta, fRVec& LHEPart_phi);

  private:

};

class DYHToverlapping {

  public:
    DYHToverlapping ();
    ~DYHToverlapping ();

    bool DYHTstitcher(iRVec& LHEPart_pdgId, fRVec& LHEPart_mass, fRVec& LHEPart_pt, fRVec& LHEPart_eta, fRVec& LHEPart_phi);

  private:

};

#endif
