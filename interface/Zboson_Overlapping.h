#ifndef Zboson_Overlapping_h
#define Zboson_Overlapping_h

// -------------------------------------------------------------------------------------- //
//                                                                                        //
//   classes ZonshellOverlap, ZboostedOverlap  ==> Used for Run 3                         //
//                                                                                        //
// -------------------------------------------------------------------------------------- //

// Standard libraries
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>


typedef const ROOT::VecOps::RVec<float> fRVec;
typedef const ROOT::VecOps::RVec<bool> bRVec;
typedef const ROOT::VecOps::RVec<int> iRVec;


class ZonshellOverlap {

  public:
    ZonshellOverlap ();
    ~ZonshellOverlap ();

    bool ZonshellStitcher(iRVec& LHEPart_pdgId, fRVec& LHEPart_pt, fRVec& LHEPart_eta, fRVec& LHEPart_phi, fRVec& LHEPart_mass);

  private:

};

class ZboostedOverlap {

  public:
    ZboostedOverlap ();
    ~ZboostedOverlap ();

    bool ZboostedStitcher(iRVec& LHEPart_pdgId, fRVec& LHEPart_pt, fRVec& LHEPart_eta, fRVec& LHEPart_phi, fRVec& LHEPart_mass);

  private:

};


#endif
