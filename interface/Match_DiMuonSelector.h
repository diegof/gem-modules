#ifndef Match_DiMuonSelector_h
#define Match_DiMuonSelector_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class Match_DiMuonSelector                                                                                   //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

typedef const ROOT::VecOps::RVec<float> fRVec;
typedef const ROOT::VecOps::RVec<bool> bRVec;
typedef const ROOT::VecOps::RVec<int> iRVec;

struct muon_output_v2 {
  std::vector<int> mu1_index;
  std::vector<int> mu2_index;
  std::vector<float> DiMuon_invM;
};

struct Z_output {
  std::vector<int> Z_mu1_idx;
  std::vector<int> Z_mu2_idx;
  std::vector<bool> Z_truth;
};


class Match_DiMuonSelector {

  public:
    Match_DiMuonSelector ();
    ~Match_DiMuonSelector ();

    muon_output_v2 GoodZMuons(
			   fRVec& Muon_pt, fRVec& Muon_tunepRelPt, fRVec& Muon_ptErr, fRVec& Muon_eta, fRVec& Muon_phi, 
			   iRVec& Muon_highPtId, fRVec& Muon_tkRelIso, fRVec& Muon_dxy, iRVec& Muon_charge );

    Z_output Zmatching(
		       iRVec& GenPart_pdgId, iRVec& GenPart_genPartIdxMotherPrompt, fRVec& GenPart_eta, 
		       fRVec& GenPart_phi, fRVec& Muon_eta, fRVec& Muon_phi, iRVec& Muon_charge );

    float deltaR(float eta1, float eta2, float phi1, float phi2);

  private:

};

#endif
