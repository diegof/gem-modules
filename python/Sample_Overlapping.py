import os
import tarfile
import tempfile

from analysis_tools.utils import import_root

ROOT = import_root()

###############################
### Old functions for Run 2 ###
###############################

class DY_Tau():
    def __init__(self, isDY, *args, **kwargs):
        self.isDY = isDY

        if self.isDY:
            if "/libGEMModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libGEMModules.so")

            base = "{}/{}/src/GEM/Modules".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/DY_Overlapping.h".format(base))
            ROOT.gInterpreter.Declare("""
                auto DYTau = DY_TauOverlapping();
            """)
    
    def run(self, df):
        if not self.isDY:
            return df, []

        df = df.Define("DY_rejectTauEvent", "DYTau.DYTauStitcher(GenPart_pdgId, GenPart_genPartIdxMotherPrompt, LHE_HT, LHEPart_pdgId, LHEPart_mass, LHEPart_pt, LHEPart_eta, LHEPart_phi)")

        df = df.Filter("DY_rejectTauEvent == false")

        return df, []


def DY_TauDecay(**kwargs):
    return lambda: DY_Tau(**kwargs)


class DY_MassSplitter():
    def __init__(self, isFirstMbin, *args, **kwargs):
        self.isFirstMbin = isFirstMbin
    
    def run(self, df):
        if not self.isFirstMbin:
            return df, []

        df = df.Filter("LHE_HT < 200")

        return df, []


def DY_MassOverlapping(**kwargs):
    return lambda: DY_MassSplitter(**kwargs)


class DY_HTStitcher():
    def __init__(self, isHTbin, *args, **kwargs):
        self.isHTbin = isHTbin

        if self.isHTbin:
            if "/libGEMModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libGEMModules.so")

            base = "{}/{}/src/GEM/Modules".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/DY_Overlapping.h".format(base))
            ROOT.gInterpreter.Declare("""
                auto DYHT = DYHToverlapping();
            """)
    
    def run(self, df):
        if not self.isHTbin:
            return df, []

        df = df.Define("DY_rejectHTevent", "DYHT.DYHTstitcher(LHEPart_pdgId, LHEPart_mass, LHEPart_pt, LHEPart_eta, LHEPart_phi)")

        df = df.Filter("DY_rejectHTevent == false")

        return df, []


def DY_HTOverlapping(**kwargs):
    return lambda: DY_HTStitcher(**kwargs)


class TTbarSplitter():
    def __init__(self, isTT, *args, **kwargs):
        self.isTT = isTT

        if self.isTT:
            if "/libGEMModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libGEMModules.so")

            base = "{}/{}/src/GEM/Modules".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/TTbar_Overlapping.h".format(base))
            ROOT.gInterpreter.Declare("""
                auto TToverlap = TTbar_Overlapping();
            """)
    
    def run(self, df):
        if not self.isTT:
            return df, []

        df = df.Define("TT_rejectEvent", "TToverlap.TTstitcher(GenPart_pdgId, GenPart_pt, GenPart_eta, GenPart_phi, GenPart_mass)")

        df = df.Filter("TT_rejectEvent == false")

        return df, []


def TTbar_Overlapping(**kwargs):
    return lambda: TTbarSplitter(**kwargs)


###############################
### New functions for Run 3 ###
###############################

class Zonshell_Stitcher():
    def __init__(self, *args, **kwargs):
        self.isZonshell = kwargs.pop("isZonshell")

        if self.isZonshell:
            if "/libGEMModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libGEMModules.so")

            base = "{}/{}/src/GEM/Modules".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/Zboson_Overlapping.h".format(base))
            ROOT.gInterpreter.Declare("""                                                                                                
                auto Zonshell = ZonshellOverlap();                                                                                   
            """)

    def run(self, df):
        if not self.isZonshell:
            return df, []

        df = df.Define("Zonshell_rejectEvent", "Zonshell.ZonshellStitcher(LHEPart_pdgId, LHEPart_pt, LHEPart_eta, LHEPart_phi, LHEPart_mass)")

        df = df.Filter("Zonshell_rejectEvent == false")

        return df, []


def Zonshell_Overlap(**kwargs):
    return lambda: Zonshell_Stitcher(**kwargs)


class Zboosted_Stitcher():
    def __init__(self, *args, **kwargs):
        self.isZboost = kwargs.pop("isZboost")

        if self.isZboost:
            if "/libGEMModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libGEMModules.so")

            base = "{}/{}/src/GEM/Modules".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/Zboson_Overlapping.h".format(base))
            ROOT.gInterpreter.Declare("""                                                                                                
                auto Zboosted = ZboostedOverlap();                                                                                   
            """)

    def run(self, df):
        if not self.isZboost:
            return df, []

        df = df.Define("Zboost_rejectEvent", "Zboosted.ZboostedStitcher(LHEPart_pdgId, LHEPart_pt, LHEPart_eta, LHEPart_phi, LHEPart_mass)")

        df = df.Filter("Zboost_rejectEvent == false")

        return df, []


def Zboosted_Overlap(**kwargs):
    return lambda: Zboosted_Stitcher(**kwargs)




