import os
from analysis_tools.utils import import_root

ROOT = import_root()


class ptZreweightProducer():
    def __init__(self, myfile, targetfile, myhist="ptZ", targethist="ptZ",
            norm=False, verbose=False, doSysVar=True, *args, **kwargs):
        self.doSysVar = doSysVar

        if "/libGEMModules.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gSystem.Load("libGEMModules.so")
        base = "{}/{}/src/GEM/Modules".format(
            os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
        ROOT.gROOT.ProcessLine(".L {}/interface/ptZreweightInterface.h".format(base))
        
        ROOT.gInterpreter.Declare(
            'auto reweighter = ptZreweightInterface("%s", "%s", "%s", "%s", %s, %s);' % (
                myfile, targetfile, myhist, targethist, ("true" if norm else "false"),
                ("true" if verbose else "false"))
        )

        if self.doSysVar:
            ROOT.gInterpreter.Declare(
                'auto reweighter_plus = ptZreweightInterface("%s", "%s", "%s", "%s", %s, %s);' % (
                myfile, targetfile, myhist, targethist + "_plus", ("true" if norm else "false"),
                ("true" if verbose else "false"))
            )
            ROOT.gInterpreter.Declare(
                'auto reweighter_minus = ptZreweightInterface("%s", "%s", "%s", "%s", %s, %s);' % (
                myfile, targetfile, myhist, targethist + "_minus", ("true" if norm else "false"),
                ("true" if verbose else "false"))
            )

    def run(self, df):
        df = df.Define("ptZ", "sqrt((Muon_tunepRelPt.at(mu1_index)*Muon_pt.at(mu1_index))*(Muon_tunepRelPt.at(mu1_index)*Muon_pt.at(mu1_index))+(Muon_tunepRelPt.at(mu2_index)*Muon_pt.at(mu2_index))*(Muon_tunepRelPt.at(mu2_index)*Muon_pt.at(mu2_index))+2*(Muon_tunepRelPt.at(mu1_index)*Muon_pt.at(mu1_index))*(Muon_tunepRelPt.at(mu2_index)*Muon_pt.at(mu2_index))*(cos(Muon_phi.at(mu1_index))*cos(Muon_phi.at(mu2_index))+sin(Muon_phi.at(mu1_index))*sin(Muon_phi.at(mu2_index))))")
        df = df.Define("ptZweight", "reweighter.get_weight(ptZ)")
        var_to_return = ["ptZ","ptZweight"]
        
        if self.doSysVar:
            df = df.Define("ptZweight_up", "reweighter_plus.get_weight(ptZ)")
            df = df.Define("ptZweight_down", "reweighter_minus.get_weight(ptZ)")
            var_to_return += ["ptZweight_up", "ptZweight_down"]

        return df, var_to_return


class ptZreweightDummyProducer():
    def run(self, df):
        return df, []


# Define modules using the syntax 'name = lambda : constructor' to avoid having them loaded when not needed

# 2016 preVFP
ptZfile_UL2016preVFP = "%s/src/GEM/Modules/data/ptZ__pg_UL16_preVFP.root" % os.environ[
    'CMSSW_BASE']
ptZweight_UL2016preVFP = lambda: ptZreweightProducer(
    ptZfile_UL2016preVFP, ptZfile_UL2016preVFP, "histograms/background", "histograms/DataUL16_preVFP", verbose=False, doSysVar=False)

# 2016 postVFP
ptZfile_UL2016postVFP = "%s/src/GEM/Modules/data/ptZ__pg_UL16_postVFP.root" % os.environ[
    'CMSSW_BASE']
ptZweight_UL2016postVFP = lambda: ptZreweightProducer(
    ptZfile_UL2016postVFP, ptZfile_UL2016postVFP, "histograms/background", "histograms/DataUL16_postVFP", verbose=False, doSysVar=False)

# 2017
ptZfile_UL2017 = "%s/src/GEM/Modules/data/ptZ__pg_UL17.root" % os.environ[
    'CMSSW_BASE']
ptZweight_UL2017 = lambda: ptZreweightProducer(
    ptZfile_UL2017, ptZfile_UL2017, "histograms/background", "histograms/DataUL17", verbose=False, doSysVar=False)

# 2018
ptZfile_UL2018 = "%s/src/GEM/Modules/data/ptZ__pg_UL18_corrected.root" % os.environ[
    'CMSSW_BASE']
ptZweight_UL2018 = lambda: ptZreweightProducer(
    ptZfile_UL2018, ptZfile_UL2018, "histograms/background", "histograms/DataUL18", verbose=False, doSysVar=False)


def ptZreweightRDF(**kwargs):
    isMC = kwargs.pop("isMC")
    year = int(kwargs.pop("year"))
    isUL = kwargs.pop("isUL")
    ul = "" if not isUL else "UL"
    ispreVFP = kwargs.pop("preVFP")
    if year == 2016:
        if ispreVFP:
            tag = "preVFP"
        else:
            tag = "postVFP"
    else:
        tag = ""

    if not isMC:
        return lambda: ptZreweightDummyProducer()
    else:
        return eval("ptZweight_%s%s%s" % (ul, year, tag))
