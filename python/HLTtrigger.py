import os
import tarfile
import tempfile

from analysis_tools.utils import import_root

ROOT = import_root()


class HLTpaths():
    def __init__(self, year, *args, **kwargs):
        self.year = year
    
    def run(self, df):
        all_branches = df.GetColumnNames()

        ### Run 2 ###

        if self.year == 2018 or self.year == 2017:
            #if "HLT_Mu50" in all_branches and "HLT_TkMu100" in all_branches and "HLT_OldMu100" in all_branches:
            #    df = df.Define("HLTpath", "HLT_Mu50 == 1 || HLT_TkMu100 == 1 || HLT_OldMu100 == 1").Filter("HLTpath")
            #elif "HLT_Mu50" in all_branches:
            #    df = df.Define("HLTpath", "HLT_Mu50 == 1").Filter("HLTpath")

            required_paths = ("HLT_Mu50","HLT_TkMu100","HLT_OldMu100")

        elif self.year == 2016:
            #if "HLT_Mu50" in all_branches and "HLT_TkMu50" in all_branches:
            #    df = df.Define("HLTpath", "HLT_Mu50 == 1 || HLT_TkMu50 == 1").Filter("HLTpath")
            #elif "HLT_Mu50" in all_branches:
            #    df = df.Define("HLTpath", "HLT_Mu50 == 1").Filter("HLTpath")

            required_paths = ("HLT_Mu50","HLT_TkMu50")

        ### Run 3 ###

        elif self.year == 2022 or self.year == 2023:
            #if "HLT_Mu50" in all_branches and "HLT_HighPtTkMu100" in all_branches and "HLT_CascadeMu100" in all_branches:
            #    df = df.Define("HLTpath", "HLT_Mu50 == 1 || HLT_HighPtTkMu100 == 1 || HLT_CascadeMu100 == 1").Filter("HLTpath")

            required_paths = ("HLT_Mu50","HLT_HighPtTkMu100","HLT_CascadeMu100")

        else:
            raise ValueError("Only implemented for RunII (years 2016, 2017 & 2018) & Run 3 2022 and 2023")

        hltpathcut="0"
        for path in required_paths:
            if path in all_branches:
                if (hltpathcut=="0"): 
                    hltpathcut = path+"==1"
                else:
                    hltpathcut += " || "+path+"==1"

        df = df.Define("HLTpath", hltpathcut).Filter("HLTpath")

        return df, []


def HLTpath(**kwargs):
    return lambda: HLTpaths(**kwargs)


