import os
import tarfile
import tempfile

from analysis_tools.utils import import_root

ROOT = import_root()


class biasRegionsRDF():
    def __init__(self, *args, **kwargs):

        ROOT.gInterpreter.Declare("""
            using Vfloat = const ROOT::RVec<float>&;

            int get_mu_region(Vfloat eta, Vfloat phi, int mu_idx) {
                int region[3];
                int reg;
                if(eta[mu_idx] < -2.1){
                   region[0] = 0; region[1] = 1; region[2] = 2;
                } else if(eta[mu_idx] < -1.2){
                   region[0] = 3; region[1] = 4; region[2] = 5;
                } else if(eta[mu_idx] < 0){
                   region[0] = 6; region[1] = 7; region[2] = 8;
                } else if(eta[mu_idx] < 1.2){
                   region[0] = 9; region[1] = 10; region[2] = 11;
                } else if(eta[mu_idx] < 2.1){
                   region[0] = 12; region[1] = 13; region[2] = 14;
                } else {
                   region[0] = 15; region[1] = 16; region[2] = 17;
                }
                double phiEdge[3];                                                                                                                        
                phiEdge[0] = -M_PI/3.0; phiEdge[1] = M_PI/3.0; phiEdge[2] = M_PI;                                                          
                unsigned int i;                                                                                                                        
                for (i = 0; i < 3; i++) {                                                                                                        
                   if (phi[mu_idx] <= phiEdge[i]) break;
                }
                return reg = region[i];
            }  
        """)
    
    def run(self, df):

        df = df.Define("mu1_region", "get_mu_region(Muon_eta, Muon_phi, mu1_index)")
        df = df.Define("mu2_region", "get_mu_region(Muon_eta, Muon_phi, mu2_index)")

        return df, ["mu1_region", "mu2_region"]


def biasRegions(**kwargs):
    return lambda: biasRegionsRDF(**kwargs)
