import os
import tarfile
import tempfile

from analysis_tools.utils import import_root

ROOT = import_root()


class DiMuonSelecRDF():
    def __init__(self, *args, **kwargs):

        if "/libGEMModules.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gSystem.Load("libGEMModules.so")

        base = "{}/{}/src/GEM/Modules".format(
            os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
        ROOT.gROOT.ProcessLine(".L {}/interface/DiMuonSelector.h".format(base))
        ROOT.gInterpreter.Declare("""
            auto DiMuSel = DiMuonSelector();
        """)
    
    def run(self, df):

        branches = ["mu1_index", "mu2_index", "kappa_mu1", "kappa_mu2", "DiMuon_invM", "DiMuon_pt"]

        df = df.Define("muon_results", "DiMuSel.GoodZMuons(Muon_pt, Muon_tunepRelPt, Muon_ptErr, Muon_eta,"
                       "Muon_phi, Muon_highPtId, Muon_tkRelIso, Muon_dxy, Muon_charge)")

        for branch in branches:
            df = df.Define(branch, "muon_results.%s" % branch)

        df = df.Filter("mu1_index != -1")


        return df, branches


def DiMuonSelec(**kwargs):
    return lambda: DiMuonSelecRDF(**kwargs)


class ZMatchProducer():
    def __init__(self, isMC, *args, **kwargs):
        self.isMC = isMC

        if "/libGEMModules.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gSystem.Load("libGEMModules.so")

        base = "{}/{}/src/GEM/Modules".format(
            os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
        ROOT.gROOT.ProcessLine(".L {}/interface/Match_DiMuonSelector.h".format(base))
        ROOT.gInterpreter.Declare("""
            auto DiMuSel = Match_DiMuonSelector();
        """)
    
    def run(self, df):
        if not self.isMC:
            return df, []

        branches = ["Z_mu1_idx", "Z_mu2_idx", "Z_truth"]

        df = df.Define("Ztruth_results", "DiMuSel.Zmatching(GenPart_pdgId, GenPart_genPartIdxMotherPrompt, GenPart_eta,"
                       "GenPart_phi, Muon_eta, Muon_phi, Muon_charge)")

        for branch in branches:
            df = df.Define(branch, "Ztruth_results.%s" % branch)


        return df, branches


def ZMatch(**kwargs):
    return lambda: ZMatchProducer(**kwargs)


