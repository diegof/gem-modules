import os
import tarfile
import tempfile

from analysis_tools.utils import import_root

ROOT = import_root()


class PromptMom():
    def __init__(self, isDY, *args, **kwargs):
        self.isDY = isDY

        if self.isDY:
            if "/libGEMModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libGEMModules.so")

            base = "{}/{}/src/GEM/Modules".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/GenPart_momPrompt.h".format(base))
            ROOT.gInterpreter.Declare("""
                auto momFinal = GenPart_momPrompt();
            """)
    
    def run(self, df):
        if not self.isDY:
            return df, []

        df = df.Define("GenPart_genPartIdxMotherPrompt", "momFinal.momPrompt(GenPart_pdgId, GenPart_genPartIdxMother)")

        return df, ["GenPart_genPartIdxMotherPrompt"]


def GenPart_momPrompt(**kwargs):
    return lambda: PromptMom(**kwargs)


