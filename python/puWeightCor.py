import os

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

json_path = "/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/LUM/{}/puWeights.json.gz"


class puWeightProducerRun2():
    def __init__(self, *args, **kwargs):
        self.year   = int(kwargs.pop("year"))
        self.isUL   = kwargs.pop("isUL")
        self.isMC   = kwargs.pop("isMC")

        if self.isMC:
            if not self.isUL:
                raise ValueError("This module can't be used for preUL, use puWeight instead")
            if self.year == 2018:
                filename = json_path.format("2018_UL")
            else:
                raise ValueError("2016 and 2017 not yet implemented")

            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")
            ROOT.gInterpreter.Declare(os.path.expandvars(
                '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))
            ROOT.gInterpreter.ProcessLine(
                'auto corr = MyCorrections("%s", "Collisions%s_UltraLegacy_goldenJSON");'
                    % (filename, str(self.year)[-2:]))

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = ['puWeight2', 'puWeightUp2', 'puWeightDown2']
        for branch_name, syst in zip(branches, ["nominal", "up", "down"]):
            df = df.Define(branch_name, 'corr.eval({Pileup_nTrueInt, "%s"})' % syst)
        return df, branches


def puWeightRun2(**kwargs):
    return lambda: puWeightProducerRun2(**kwargs)


class puWeightProducerRun3():
    def __init__(self, *args, **kwargs):
        self.year   = int(kwargs.pop("year"))
        self.isMC   = kwargs.pop("isMC")
        self.runPeriod = kwargs.pop("runPeriod")

        if self.isMC:
            if self.year == 2022:
                if self.runPeriod == "postEE":
                    filename = json_path.format("2022_Summer22EE")
                    golden   = "Collisions2022_359022_362760_eraEFG_GoldenJson"
                elif self.runPeriod == "preEE":
                    filename = json_path.format("2022_Summer22")
                    golden   = "Collisions2022_355100_357900_eraBCD_GoldenJson"
                else:
                    raise ValueError("Should be 2 periods until 2022 ReReco")
            elif self.year == 2023:
                if self.runPeriod == "postBPix":
                    filename = json_path.format("2023_Summer23BPix")
                    golden = "Collisions2023_369803_370790_eraD_GoldenJson"
                elif self.runPeriod == "preBPix":
                    filename = json_path.format("2023_Summer23")
                    golden = "Collisions2023_366403_369802_eraBC_GoldenJson"
                else:
                    raise ValueError("Should be 2 periods until 2023 ReReco")
            else:
                raise ValueError("Only 2022 an 2023 implemented for the moment")

            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")
            ROOT.gInterpreter.Declare(os.path.expandvars(
                '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))
    
            ROOT.gInterpreter.ProcessLine(
                'auto corr = MyCorrections("%s", "%s");'
                    % (filename, golden) )

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = ['puWeight', 'puWeight_up', 'puWeight_down']
        for branch_name, syst in zip(branches, ["nominal", "up", "down"]):
            df = df.Define(branch_name, 'corr.eval({Pileup_nTrueInt, "%s"})' % syst)

        return df, branches


def puWeightRun3(**kwargs):
    return lambda: puWeightProducerRun3(**kwargs)
